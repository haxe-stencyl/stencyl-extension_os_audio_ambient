package extension.iosaudio;

import haxe.macro.Expr;
import haxe.macro.Context;
class StencylEngineModifier
{
	#if macro
	public static function build()
	{
	haxe.macro.Compiler.addMetadata("@:build(extension.iosaudio.StencylEngineModifier.changeMain())", "com.stencyl.Config");// 

	
	}
	#end
	

	public static function changeMain():Array<Field>
	{
		var fields:Array<Field> = Context.getBuildFields();

		#if ios

		for(field in fields){

				if (field.name == "load")
				{
					switch (field.kind){
						case FieldType.FFun(func): 
							switch (func.expr.expr)
							{
								case EBlock(a) : 
									trace("IOSAudioExtension : Modifying load in Config to include 'enableAmbient' ...");
									var newC = "#if ios
									extension.iosaudio.IOSAudio.enableAmbient();
									#end";
									var oldC = haxe.macro.ExprTools.toString(func.expr);
									var tc = "{" + newC +oldC.substr(1);
									var c = Context.parse(tc, func.expr.pos );
									func.expr = c;
								default: 
							}						
						default:
					}
				}
		}

		#end

		return fields;
	}
}